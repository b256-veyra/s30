// Objective 1
// Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$count: "totalNumbersOfFruitOnSale"},
    {$out: "totalNumberOfFruitsOnSale"},
]);

// Objective 2
// Use the count operator to count the total number of fruits with stock more than or equal to 20
db.fruits.aggregate([
    {$match: {$and: [
        {stock: {$gte: 20}}
        ]}},
        {$count: "enoughStock"},
        {$out: "totalNumberOfFruitsWithStockMoreThanOrEqualTo20"}       
]);



// Objective 3
// Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}},
    {$out: "averagePriceOfFruitsOnSalePerSupplier"}
]);




// Objective 4
// Use the max operator to get the highest price of a fruit per supplier
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}},
    {$out: "highestPriceOfAFruitPerSupplier"}
])



// Objective 5
// Use the min operator to get the lowest price of a fruit per supplier
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}},
    {$out: "lowestPriceOfAFruitPerSupplier"}
])



